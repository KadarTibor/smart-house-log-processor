package PT2017.PT2017.Homework5.model;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

public class MonitoredData {
	public LocalDateTime startTime;
	public LocalDateTime endTime;
	public String activityLabel;
	
	public MonitoredData(String line){
		//split the line from the file into 3, first date, second date, and activity label
       String[] parameters = line.split("		");
       startTime = getDateFromString(parameters[0]);
       endTime = getDateFromString(parameters[1]);
       activityLabel = parameters[2];
       
	}
	
	@SuppressWarnings("deprecation")
	private LocalDateTime getDateFromString(String str){
		//splits the string into two date and time
		String[] dateAndTime = str.split(" ");
		//get the strings for the date
		String[] date = dateAndTime[0].split("-");
		//get the strings for the time
		String[] time = dateAndTime[1].split(":");
		int year = Integer.parseInt(date[0]);
		int month = Integer.parseInt(date[1]) - 1;
		int day = Integer.parseInt(date[2]);
		int hour = Integer.parseInt(time[0]);
		int min = Integer.parseInt(time[1]);
		int sec = Integer.parseInt(time[2]);
		return  LocalDateTime.of(year,month,day,hour,min,sec);
		
	}
	
	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	
}
