package PT2017.PT2017.Homework5;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import PT2017.PT2017.Homework5.Operations.Operations;
import PT2017.PT2017.Homework5.fileIO.ReadFile;
import PT2017.PT2017.Homework5.fileIO.WriteFile;
import PT2017.PT2017.Homework5.model.MonitoredData;

public class App 
{
    public static void main( String[] args )
    {
    
    	@SuppressWarnings("resource")
    	Scanner input = new Scanner(System.in);
       
       System.out.println("Stream processing program");
       System.out.println("Select an item from the menu:");
       System.out.println("1. Count the distinct days that appear in the monitoring data.");
       System.out.println("2. Map each distinct action type the number of occurrences in the log.");
       System.out.println("3. Map the activity count for each day of the log.");
       System.out.println("4. Map for each activity the total duration computed over the monitoring period. Filter the activities with total duration larger than 10 hour.");
       System.out.println("5. Filter the activities that have 90% of the monitoring samples with duration less than 5 minutes.");
       System.out.println("6. Exit.");
       
       System.out.println("1. --> enter nr 1");
       System.out.println("2. --> enter nr 2");
       System.out.println("3. --> enter nr 3");
       System.out.println("4. --> enter nr 4");
       System.out.println("5. --> enter nr 5");
       System.out.println("6. --> enter exit");
       
       String selection = "1";
       
       Operations operation = new Operations();
       ReadFile rdf = new ReadFile("src/Activities.txt");
       rdf.openFile();
      
       List<MonitoredData> inList = rdf.read();
       
       rdf.closeFile();
       WriteFile wrf = new WriteFile("src/Task1.txt");
       
       while(!selection.equals("exit")){
    	   System.out.println("Enter your wish");
    	   selection = input.next();
    	   switch(selection){
    	   case "1" : 
    		   			wrf.setOutFile("src/Task1.txt");
    		   			wrf.openFile();
    		   			List<LocalDateTime>  result1 = operation.distinctDays(inList);
    		   			result1.stream().forEach(a -> wrf.write(a.getYear() +"-" +a.getMonth()+"-"+a.getDayOfMonth()));
    		   			wrf.write("The number of distinct days is: " +result1.size());
    		   			wrf.closeFile();
    		   			ProcessBuilder pb1 = new ProcessBuilder("Notepad.exe", "src/Task1.txt");
						try {
							pb1.start();
						} catch (IOException e) {
							
							e.printStackTrace();
						}
    		   			break;
    		   		
    	   case "2" :
    		   			wrf.setOutFile("src/Task2.txt");
    		   			wrf.openFile();
    		   			Map<String,Integer> result2 = operation.countActions(inList);
    		   			wrf.write("Activities and their counts:");
    		   			result2.entrySet().stream().forEach(map -> {wrf.write(map.getKey().trim() + "---" + map.getValue());});
    		   			wrf.closeFile();
    		   			ProcessBuilder pb2 = new ProcessBuilder("Notepad.exe", "src/Task2.txt");
						try {
							pb2.start();
						} catch (IOException e) {
							
							e.printStackTrace();
						}
    		   			break;
    	   case "3" :   
    		   			wrf.setOutFile("src/Task3.txt");
  						wrf.openFile();
  						wrf.write("Activities performed each day:");
  						Map<LocalDateTime,Map<String,Integer>> result3 = operation.countActionsPerDay(inList);
  						result3.entrySet().stream().forEach(map ->{ wrf.write(map.getKey().getYear() +"-" +map.getKey().getMonth()+"-"+map.getKey().getDayOfMonth());
  																	Map<String,Integer> a = map.getValue();
  																	for(Map.Entry<String,Integer> b:a.entrySet()){
  																		wrf.write("         " + b.getKey().trim() + "--" + b.getValue());  																		  																	}}); 
  						wrf.closeFile();
  						ProcessBuilder pb3 = new ProcessBuilder("Notepad.exe", "src/Task3.txt");
						try {
							pb3.start();
						} catch (IOException e) {
							
							e.printStackTrace();
						}
  						break;
    	   case "4" :	
    		   			wrf.setOutFile("src/Task4.txt");
    		   			wrf.openFile();
    		   			Map<String,Double> result4 = operation.duration(inList);
    		   			result4.entrySet().stream().forEach(map -> {wrf.write(map.getKey()+"       "+ map.getValue());});
    		   			wrf.closeFile();
    		   			ProcessBuilder pb4 = new ProcessBuilder("Notepad.exe", "src/Task4.txt");
						try {
							pb4.start();
						} catch (IOException e) {
							
							e.printStackTrace();
						}
    		   			break;
    	   case "5" :	
    		   			wrf.setOutFile("src/Task5.txt");
  						wrf.openFile();
  						List<String> result5 = operation.shortActivities(inList);
  						result5.stream().forEach(a -> wrf.write(a));
  						wrf.closeFile();
  						ProcessBuilder pb5 = new ProcessBuilder("Notepad.exe", "src/Task5.txt");
						try {
							pb5.start();
						} catch (IOException e) {
							
							e.printStackTrace();
						}
  						break;
    	   case "exit" :
    		   			break;
    	   default: break;
    	   }
       }
    }
}
