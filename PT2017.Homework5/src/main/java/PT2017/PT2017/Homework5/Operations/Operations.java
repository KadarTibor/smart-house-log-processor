package PT2017.PT2017.Homework5.Operations;


import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;



import PT2017.PT2017.Homework5.model.MonitoredData;

public class Operations {
	/**
	 * maps each monitored data object to a new date object, then filter them to be distinct and lastly put them in a list
	 * @param inList
	 * @return
	 */
	public List<LocalDateTime> distinctDays(List<MonitoredData> inList){
		List<LocalDateTime> result = inList.stream().map( a ->  LocalDateTime.of(a.getStartTime().getYear(),a.getStartTime().getMonth(),a.getStartTime().getDayOfMonth(),0,0,0))	
													.distinct()
													.collect(Collectors.toList());
		
		return result;
	}
	/**
	 * returns the map using the collectors groupingby the key is the activity label and the value is the counting of the activity
	 * @param inList
	 * @return
	 */
	public Map<String,Integer> countActions(List<MonitoredData> inList){
		Map<String,Integer> result = inList.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.reducing(0, e -> 1, Integer::sum)));
		return result;
	}
	/**
	 * using the same technique as for the countActions we take that as value and as key we take the distinct days
	 * @param inList
	 * @return
	 */
	public Map<LocalDateTime,Map<String,Integer>>  countActionsPerDay(List<MonitoredData> inList){
		Map<LocalDateTime,Map<String,Integer>> result = inList.stream().collect(Collectors.groupingBy(date -> LocalDateTime.of(date.getStartTime().getYear(),date.getStartTime().getMonthValue(),date.getStartTime().getDayOfMonth(),0,0,0),Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.reducing(0, e -> 1, Integer::sum))));
		return result;
	}
	/**
	 * map the activity label as key and the sum of the duration of each occurance as value only the actions whichi lasted lnnger than 10 hours are chosen
	 * @param inList
	 * @return
	 */
	public Map<String,Double> duration(List<MonitoredData> inList){
		Map<String,Double> result = inList.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.summingDouble( e-> (e.getEndTime().toEpochSecond(ZoneOffset.ofTotalSeconds(0)) - e.getStartTime().toEpochSecond(ZoneOffset.ofTotalSeconds(0)))/3600)));
		result = result.entrySet().stream().filter(map -> map.getValue() > 10).collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
		return result;
	}
	/**
	 * count the occurrence of each action
	 * extract the good activities and count their occurrence only the activities with smaller duration than 5 minutes
	 * calculate the percentage of the good occurrences and put them into the list
	 * 
	 * @param inList
	 * @return
	 */
	public  List<String> shortActivities(List<MonitoredData> inList){
		Map<String,Integer> activities = countActions(inList);
		Map<String,Integer> goodActivities = inList.stream().filter(e -> (e.getEndTime().toEpochSecond(ZoneOffset.ofTotalSeconds(0)) - e.getStartTime().toEpochSecond(ZoneOffset.ofTotalSeconds(0)))/60 <= 5).collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.reducing(0, e -> 1, Integer::sum)));
		goodActivities = goodActivities.entrySet().stream().filter(map ->{int total =  activities.get(map.getKey()); 
														  if(map.getValue() >= 0.9*total)
															  return true;
														  else
															  return false;})
				.collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
		return goodActivities.entrySet().stream().map(map-> map.getKey()).collect(Collectors.toList());
	}
	
}

