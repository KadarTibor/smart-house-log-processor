package PT2017.PT2017.Homework5.fileIO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class WriteFile {
	private String outFile;
	private PrintWriter output;
	
	public WriteFile(String outFile){
		this.outFile = outFile;
	}
	
	public void openFile(){
		try
		{
			this.output = new PrintWriter( new FileWriter(new File(this.outFile)));
		}
		catch(IOException e)
		{
			System.out.println("Problem opening the file");
		}
	}
	
	public void write(String str){
		output.println(str);
	}
	
	public void write(int nr){
		output.println(nr);
	}
	
	public void closeFile(){
		output.close();
	}
	
	public String getOutFile() {
		return outFile;
	}

	public void setOutFile(String outFile) {
		this.outFile = outFile;
	}
}
