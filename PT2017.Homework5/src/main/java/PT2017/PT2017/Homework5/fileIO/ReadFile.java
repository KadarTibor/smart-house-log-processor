package PT2017.PT2017.Homework5.fileIO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import PT2017.PT2017.Homework5.model.MonitoredData;

public class ReadFile {
	private String inFile;
	private Scanner input;
	
	public ReadFile(String inFile){
		this.inFile = inFile;
	}
	
	public void openFile(){
		try
		{
			this.input = new Scanner(new BufferedReader(new FileReader(new File(this.inFile))));
		}
		catch(IOException e)
		{
			System.out.println("Problem opening the file");
		}
	}
	
	public List<MonitoredData> read(){
		List<MonitoredData> result = new ArrayList<MonitoredData>();
		
		while(input.hasNext()){
			String line = input.nextLine();
			result.add(new MonitoredData(line));
		}
		return result;
	}
	
	public void closeFile(){
		input.close();
	}
	
	public String getInFile() {
		return inFile;
	}

	public void setInFile(String inFile) {
		this.inFile = inFile;
	}
	
	
}
